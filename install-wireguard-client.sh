e#!/bin/bash
#https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-ubuntu-20-04
#run as root

SERVER_IP=10.0.1.1
DEFAULT_NETWORK_INTERFACE=`ip route | awk '/default/ { print $5 }'`
WIREGUARD_PORT=60000
TUNNEL_DEVICE=wg0

apt update

apt install wireguard -y

wg genkey | sudo tee /etc/wireguard/private.key

#sudo chmod go=... command removes any permissions on the file for users and groups other than the root user to ensure that only it can access the private key.
chmod go= /etc/wireguard/private.key

cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key


printf "[Interface]\n\
PrivateKey = `cat /etc/wireguard/private.key`\n\
Address = ${SERVER_IP}/24\n\
ListenPort = ${WIREGUARD_PORT}\n\
SaveConfig = true\n\
PostUp = ufw route allow in on ${TUNNEL_DEVICE} out on ${DEFAULT_NETWORK_INTERFACE}\n\
PostUp = iptables -t nat -I POSTROUTING -o ${DEFAULT_NETWORK_INTERFACE} -j MASQUERADE\n\
PostUp = ip6tables -t nat -I POSTROUTING -o ${DEFAULT_NETWORK_INTERFACE} -j MASQUERADE\n\
PreDown = ufw route delete allow in on ${TUNNEL_DEVICE} out on ${DEFAULT_NETWORK_INTERFACE}\n\
PreDown = iptables -t nat -D POSTROUTING -o ${DEFAULT_NETWORK_INTERFACE} -j MASQUERADE\n\
PreDown = ip6tables -t nat -D POSTROUTING -o ${DEFAULT_NETWORK_INTERFACE} -j MASQUERADE\n\


" > /etc/wireguard/$TUNNEL_DEVICE.conf

#SaveConfig line ensures that when a WireGuard interface is shutdown, any changes will get saved to the configuration file

#If you are using WireGuard to connect a peer to the WireGuard Server in order to access services on the server only, then refer Step 4 — Adjusting the WireGuard Server’s Network Configuration
# in source url as above

ufw allow $WIREGUARD_PORT/udp

#restart ufw
ufw disable
ufw enable

#use systemd service. would boot at startup
systemctl enable wg-quick@$TUNNEL_DEVICE.service

#start the service
systemctl start wg-quick@$TUNNEL_DEVICE.service

echo "you may check status: systemctl status wg-quick@${TUNNEL_DEVICE}.service"