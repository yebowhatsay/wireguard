#!/bin/bash
#https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-ubuntu-20-04
#run as root

WIREGUARD_PORT=60000
TUNNEL_DEVICE=wg0

#stop the service
systemctl stop wg-quick@$TUNNEL_DEVICE.service

#use systemd service. would boot at startup
systemctl disable --now wg-quick@$TUNNEL_DEVICE.service

ufw deny $WIREGUARD_PORT/udp
ufw disable
ufw enable

rm /etc/wireguard/$TUNNEL_DEVICE.conf

rm /etc/wireguard/private.key
rm /etc/wireguard/public.key

apt remove wireguard