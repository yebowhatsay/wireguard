#!/bin/bash
#https://www.digitalocean.com/community/tutorials/how-to-install-tinc-and-set-up-a-basic-vpn-on-ubuntu-18-04
apt update

apt install tinc

NETNAME="doctornet"
THIS_SERVER_NAME="oldubuntu"
INTERFACE="tn1"
THIS_SERVER_PUBLIC_IP="wcmp.actmedical.com.au"
THIS_SERVER_SUBNET="10.0.1.1/32"
IP_RANGE="10.0.1.0/24"
mkdir -p /etc/tinc/$NETNAME/hosts

printf "Name = ${THIS_SERVER_NAME}\n\
AddressFamily = ipv4\n\
Interface = ${INTERFACE}\n\
" > /etc/tinc/$NETNAME/tinc.conf

printf "Address = ${THIS_SERVER_PUBLIC_IP}\n\
Subnet = ${THIS_SERVER_SUBNET}
" > /etc/tinc/$NETNAME/hosts/$THIS_SERVER_NAME

tincd -n $NETNAME -K4096


#
#    ip link …: sets the status of tinc’s virtual network interface as up
#    ip addr …: adds the IP address 10.0.0.1 with a netmask of 32 to tinc’s virtual network interface, which will cause the other machines on the VPN to see server-01’s IP address as 10.0.0.1
#    ip route …: adds a route (10.0.0.0/24) which can be reached on tinc’s virtual network interface

printf "
#!/bin/sh\n\
ip link set $INTERFACE up\n\
ip addr add ${THIS_SERVER_SUBNET} dev $INTERFACE\n\
ip route add ${IP_RANGE} dev $INTERFACE\n\
" > /etc/tinc/$NETNAME/tinc-up


printf "
#!/bin/sh\n\
ip route del ${IP_RANGE} dev $INTERFACE\n\
ip addr del ${THIS_SERVER_SUBNET} dev $INTERFACE\n\
ip link set $INTERFACE down\n\
" > /etc/tinc/$NETNAME/tinc-down

chmod 755 /etc/tinc/$NETNAME/tinc-*

ufw allow 655